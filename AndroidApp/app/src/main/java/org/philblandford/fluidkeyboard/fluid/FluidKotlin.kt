package org.philblandford.fluidkeyboard.fluid

import android.app.Application
import android.content.Context
import android.util.Log
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.HashMap

/**
 * Created by philb on 07/06/18.
 */

object FluidWrapper {
  private val TAG = "FLUIDWRAPPER"
  private val DEFAULT_SF = "chaos"
  private var currentSFPath = ""

  fun initState(context: Context) {
    try {
      releaseAssets()
      initSoundFonts(getSoundFonts(context))
      currentSFPath = context.cacheDir.toString() + "/" + DEFAULT_SF + ".sf2"
    } catch (e: IOException) {
      Log.e(TAG, "Could not get sound fonts", e)
    }

  }

  fun init(context: Context) {
    System.loadLibrary("native-lib")
    initState(context)
  }

  fun shutDown() {
    releaseAssets()
  }

  fun getPresets(name: String, context: Context): Array<Preset> {
    try {
      val path = getPath(name + ".sf2", context)
      return getPresetsNative(path)
    } catch (e: IOException) {

      Log.e(TAG, "Could not load presets", e)
      return arrayOf()
    }

  }

  @Throws(IOException::class)
  fun loadFile(file: File) {
    loadFileNative(file.absolutePath)
  }

  fun play() {
    playFileNative()
  }

  fun playNote(midiVal: Int, velocity: Int, channel: Int) {
    Log.v(TAG, String.format("playNote %d %d %d", midiVal, velocity, channel))
    playNoteNative(midiVal, velocity, channel)
  }

  fun setVelocity(velocity: Int, channel: Int) {
    setVelocityNative(velocity, channel)
  }

  fun setProgram(bank: Int, channel: Int, program: Int) {
    // fluid program numbers start at 0, ours start at 1
    Log.v(TAG, String.format("setProgram %d %d %d", bank, channel, program))
    setProgramNative(currentSFPath, bank, channel, program)
  }


  fun stop() {
    stopFileNative()
  }

  fun stopAllNotes(channel: Int) {
    stopAllNotesNative(channel)
  }

  fun assignSoundFonts(map: Map<Int, String>, context: Context) {
    for ((key, value) in map) {
      assignSoundFontNative(key, fullSoundFontPath(value, context))
    }
  }

  fun assignSoundFont(channel: Int, soundFont: String, context: Context) {
    assignSoundFontNative(channel, fullSoundFontPath(soundFont, context))
  }

  fun pauseState() {
    unloadDriver()
  }

  fun resumeState() {
    loadDriver()
  }

  private fun fullSoundFontPath(name: String?, context: Context): String {
    return if (name == null || name.isEmpty()) {
      context.cacheDir.toString() + "/" + DEFAULT_SF + ".sf2"
    } else {
      context.cacheDir.toString() + "/" + name + ".sf2"
    }
  }

  private external fun getPresetsNative(soundFontPath: String): Array<Preset>

  private external fun initSoundFonts(soundFonts: Array<String>)

  private external fun releaseAssets()

  private external fun loadFileNative(fullPath: String)

  private external fun playFileNative()

  private external fun stopFileNative()

  private external fun playNoteNative(midiVal: Int, velocity: Int, channel: Int)

  private external fun setVelocityNative(velocity: Int, channel: Int)

  private external fun setProgramNative(soundFont: String, bank: Int, channel: Int, program: Int)

  private external fun stopNoteNative(midiVal: Int, channel: Int)

  private external fun stopAllNotesNative(channel: Int)

  private external fun assignSoundFontNative(channel: Int, soundFontPath: String)

  private external fun unloadDriver()

  private external fun loadDriver()

  @Throws(IOException::class)
  private fun getPath(name: String, context: Context): String {
    val file = extractAsset(name, context)
    return file.absolutePath
  }

  @Throws(IOException::class)
  fun getSoundFonts(context: Context): Array<String> {
    return getSoundFontAssets(context.assets.list(""), context)
  }

  @Throws(IOException::class)
  private fun getSoundFontAssets(fileList: Array<String>?, context: Context): Array<String> {

    if (fileList == null) {
      return arrayOf()
    }

    val fullPaths = ArrayList<String>()
    for (file in fileList) {
      if (FilenameUtils.getExtension(file) == "sf2") {
        val fullPath = getPath(file, context)
        fullPaths.add(fullPath)
      }
    }
    return fullPaths.toTypedArray()
  }

  @Throws(IOException::class)
  private fun extractAsset(name: String, context: Context): File {
    val f = File(context.cacheDir.toString() + "/" + name)

    val `is` = context.assets.open(name)
    FileUtils.copyInputStreamToFile(`is`, f)
    return f
  }

}
