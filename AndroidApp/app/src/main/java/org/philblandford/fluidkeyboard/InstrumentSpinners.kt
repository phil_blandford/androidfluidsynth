package org.philblandford.fluidkeyboard

import android.content.Context
import android.view.View
import android.view.ViewManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.spinner
import org.philblandford.fluidkeyboard.fluid.FluidWrapper

/**
 * Created by philb on 07/06/18.
 */

var group: Group? = null
var instrument: Instrument? = null

fun ViewManager.instrumentLayout(ctx: Context, groups: Array<Group>?) = linearLayout {

  group = groups?.firstOrNull()
  instrument = group?.instruments?.firstOrNull()

  val groupSpinner = spinner()
  val instrumentSpinner = spinner()


  fun setInstrumentSpinner() {
    instrumentSpinner.adapter = ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1,
        group?.instruments?.map { it.name } ?: listOf())
  }

  groupSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
      group = groups?.get(p2)
      setInstrumentSpinner()
    }
  }

  instrumentSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
      instrument = group?.instruments?.get(p2)
      if (group?.name != "Percussion")
        FluidWrapper.setProgram(0, 0, instrument?.program ?: 0)
      else
        FluidWrapper.setProgram(0, 9, 0)
    }
  }

  groupSpinner.adapter = ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1,
      groups?.map { it.name })
  setInstrumentSpinner()


}