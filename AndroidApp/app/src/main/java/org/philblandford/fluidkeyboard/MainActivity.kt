package org.philblandford.fluidkeyboard

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.*
import org.philblandford.fluidkeyboard.fluid.FluidWrapper

class MainActivity : AppCompatActivity() {

  var instrumentGroups: Array<Group>? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    FluidWrapper.init(this)
    FluidWrapper.setProgram(0, 9, 0)
    instrumentGroups = readInstruments(this)
    MainLayout<MainActivity>().setContentView(this)
  }


 inner class MainLayout<T> : AnkoComponent<T> {

    override fun createView(ui: AnkoContext<T>) = with(ui) {
      verticalLayout {
        instrumentLayout(ctx, instrumentGroups).lparams(matchParent, 0, weight = 2f)
        space().lparams(matchParent, 0, weight = 1f)
        keyboardImage({ if (group?.name == "Percussion") 9 else 0}).lparams(matchParent, 0, weight = 4f)

      }
    }
  }
}