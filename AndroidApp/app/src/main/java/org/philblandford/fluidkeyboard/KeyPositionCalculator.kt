package org.philblandford.fluidkeyboard

import android.util.Log
import java.util.TreeMap

/**
 * Created by philb on 13/04/16.
 */
class KeyPositionCalculator {

  fun calculateMidiVal(x: Float, y: Float, totalWidth: Float, totalHeight: Float): Int {
    val relX = x / totalWidth * (sLastPos + 1)
    Log.v("KPC", "Relx " + relX + " " + sLastPos + " " + x / totalWidth)
    if (y > totalHeight * 0.5) {
      return sWhiteNoteStartMap.floorEntry(relX).value
    } else {
      val floorKey = sBlackNoteStartMap.floorKey(relX)
      if (floorKey < relX - 1f) {
        val ceiling = sBlackNoteStartMap.ceilingEntry(relX)
        return if (ceiling != null) {
          ceiling.value
        } else {
          sBlackNoteStartMap.floorEntry(relX).value
        }
      } else {
        val floor = sBlackNoteStartMap.floorEntry(relX)
        return if (floor != null) {
          sBlackNoteStartMap.floorEntry(relX).value
        } else {
          sBlackNoteStartMap.ceilingEntry(relX).value
        }
      }
    }
  }

  fun getNotePosition(midiVal: Int, totalWidth: Float): Float {
    var pos = 1.0f
    for ((key, value) in sWhiteNoteStartMap) {
      if (value == midiVal) {
        pos = key
      }
    }
    for ((key, value) in sBlackNoteStartMap) {
      if (value == midiVal) {
        pos = key
      }
    }
    return pos / sLastPos * totalWidth
  }

  companion object {
    private val sWhiteNoteStartMap = TreeMap<Float, Int>()
    private val sBlackNoteStartMap = TreeMap<Float, Int>()
    private var sLastPos: Float = 0.toFloat()

    init {
      val sNotePositionsFromC = booleanArrayOf(true, false, true, false, true, true, false, true, false, true, false, true)

      var lastWhite = true
      var pos = -1f
      for (note in 21..108) {
        val white = sNotePositionsFromC[note % 12]
        if (white) {
          pos += if (lastWhite) 1f else 0.5f
          sWhiteNoteStartMap[pos] = note
        } else {
          pos += 0.5f
          sBlackNoteStartMap[pos] = note
        }
        Log.v("KPC", "white $white pos $pos note $note")
        lastWhite = white
        sLastPos = pos
      }
    }
  }
}
