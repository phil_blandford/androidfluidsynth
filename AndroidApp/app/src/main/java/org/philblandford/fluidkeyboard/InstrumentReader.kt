package org.philblandford.fluidkeyboard

import android.content.Context
import android.content.res.AssetManager
import org.apache.commons.io.IOUtils
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors


/**
 * Created by philb on 07/06/18.
 */

data class Instrument(val name:String, val program:Int)
data class Group(val name:String, var instruments:Array<Instrument>)

fun readInstruments(ctx: Context):Array<Group> {

  val assetManager = ctx.getAssets()
  val ims = assetManager.open("instruments.txt")

  val lines = IOUtils.toString(ims).split("\n")
  var groups = arrayOf<Group>()
  var currentGroup:Group? = null

  var idx = 0
  for (line in lines) {
    if (line.contains(':')) {
      if (currentGroup != null) {
        groups += currentGroup
      }
      currentGroup = Group(line.substring(0, line.length-1), arrayOf())
    } else {
      currentGroup = currentGroup?.copy(instruments = currentGroup.instruments + Instrument(line, idx))
      idx += 1
    }
  }
  if (currentGroup != null) {
    groups += currentGroup
  }
  return groups
}