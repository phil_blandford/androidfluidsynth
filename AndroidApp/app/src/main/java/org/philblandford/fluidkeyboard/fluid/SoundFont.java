package org.philblandford.fluidkeyboard.fluid;

import java.util.List;

/**
 * Created by philb on 16/01/18.
 */

public class SoundFont {
    public String name;
    List<Preset> presets;

    public SoundFont(String name, List<Preset> presets) {
        this.name = name;
        this.presets = presets;
    }
}
