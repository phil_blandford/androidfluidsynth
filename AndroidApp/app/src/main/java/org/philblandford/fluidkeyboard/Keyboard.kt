package org.philblandford.fluidkeyboard

import android.util.Log
import android.view.View
import android.view.ViewManager
import android.view.ViewTreeObserver
import android.widget.ImageView
import org.philblandford.fluidkeyboard.KeyPositionCalculator
import org.jetbrains.anko.*
import org.philblandford.fluidkeyboard.R
import org.philblandford.fluidkeyboard.fluid.FluidWrapper
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.xml.datatype.DatatypeConstants.SECONDS



/**
 * Created by philb on 24/01/18.
 */

fun ViewManager.keyboardImage(getChannel:()->Int) = horizontalScrollView {
  val kpc = KeyPositionCalculator()

  val scheduler = Executors.newSingleThreadScheduledExecutor()

  val vl = verticalLayout {
    imageView(R.drawable.keys_nice) {
      adjustViewBounds = true
      scaleType = ImageView.ScaleType.FIT_END

      setOnTouchListener { v, event ->
        val midi = kpc.calculateMidiVal(event.x, event.y, width.toFloat(), height.toFloat())
        Log.v("KPC", "Midi " + midi)
        FluidWrapper.playNote(midi, 100, getChannel())
        val task = Runnable {
          FluidWrapper.playNote(midi, 0, getChannel())
        }
        scheduler.schedule(task, 250L, TimeUnit.MILLISECONDS)
        false
      }

    }.lparams(wrapContent, 0 ,weight = 5f)

    frameLayout { }.lparams(matchParent, 0, weight = 1f).setBackgroundResource(R.drawable.cheap_diagonal_fabric)
  }.lparams(wrapContent, wrapContent)


  waitForLayout {
    val startScroll = kpc.getNotePosition(55, vl.width.toFloat())
    smoothScrollTo(startScroll.toInt(), 0)
  }
}


inline fun View.waitForLayout(crossinline f: () -> Unit) {
  viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
    override fun onGlobalLayout() {
      viewTreeObserver.removeOnGlobalLayoutListener(this)
      f()
    }
  })
}