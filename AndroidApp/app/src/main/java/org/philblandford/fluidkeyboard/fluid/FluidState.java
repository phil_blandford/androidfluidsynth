package org.philblandford.fluidkeyboard.fluid;

/**
 * Created by philb on 14/01/18.
 */

public class FluidState {
    int settingsPtr;
    int synthPtr;
    int driverPtr;
    int playerPtr;

    public FluidState(int settingsPtr, int synthPtr, int driverPtr, int playerPtr) {
        this.settingsPtr = settingsPtr;
        this.synthPtr = synthPtr;
        this.driverPtr = driverPtr;
        this.playerPtr = playerPtr;
    }
}
