package org.philblandford.fluidkeyboard.fluid;

/**
 * Created by philb on 16/01/18.
 */

public class Preset {
    public String name;
    public int bank;
    public int number;

    public Preset(String name, int bank, int number) {
        this.name = name;
        this.bank = bank;
        this.number = number;
    }

    @Override
    public String toString() {
        return "" + name + " " + bank + " " + number;
    }
}
