#include <jni.h>
#include <string>
#include <vector>

#include "fluidsynth.h"
#include <android/log.h>

#define TAG "FSJNI"
#define CLASS_DIR "org/philblandford/fluidkeyboard/fluid/"
#undef LOG_ENTRY
#ifdef LOG_ENTRY
#define ENTRY  __android_log_print(ANDROID_LOG_INFO, "%s", __FUNCTION__);
#else
#define ENTRY
#endif

static fluid_settings_t *fluid_settings;
static fluid_synth_t *fluid_synth;
static fluid_audio_driver_t *fluid_audio_driver;
static fluid_player_t *fluid_player;


extern "C"


const char *getClass(const char *name) {
    return (std::string(CLASS_DIR) + std::string(name)).c_str();
}

void initFluidState(JNIEnv *env, jobjectArray soundFonts_) {

    if (fluid_settings != NULL)
        return;

    fluid_settings = new_fluid_settings();
    fluid_settings_setint(fluid_settings, "audio.period-size", 1024);
    fluid_settings_setint(fluid_settings, "audio.periods", 32);
    fluid_settings_setint(fluid_settings, "player.reset-synth", 0);
    fluid_settings_setint(fluid_settings, "synth.verbose", 1);
    fluid_synth = new_fluid_synth(fluid_settings);
    fluid_player = new_fluid_player(fluid_synth);
    fluid_audio_driver = new_fluid_audio_driver(fluid_settings, fluid_synth);

    jsize numRows = env->GetArrayLength(soundFonts_);
    std::vector<char *> soundFonts(0);

    for (int i = 0; i < numRows; i++) {
        jstring soundFont_ = (jstring) env->GetObjectArrayElement(soundFonts_, i);
        const char *soundFont = env->GetStringUTFChars(soundFont_, 0);
        __android_log_print(ANDROID_LOG_DEBUG, TAG, "Loaded soundfont %s", soundFont);
        fluid_synth_sfload(fluid_synth, soundFont, JNI_FALSE);
        env->ReleaseStringUTFChars(soundFont_, soundFont);
    }
}


extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_playNoteNative(JNIEnv *env, jobject instance, jint midiVal,
                                       jint velocity, jint channel) {
    ENTRY
    fluid_synth_noteon(fluid_synth, channel, midiVal, velocity);
}


extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_setProgramNative(JNIEnv *env, jobject instance,
                                         jstring soundFont_, jint bank,
                                         jint channel,
                                         jint program) {
    ENTRY
    const char *soundFont = env->GetStringUTFChars(soundFont_, 0);

    __android_log_print(ANDROID_LOG_VERBOSE, TAG, "c %d s %s b %d p %d",
                        channel, soundFont, bank, program);
    int status = fluid_synth_program_select_by_sfont_name(fluid_synth, channel, soundFont,
                                                          (unsigned int) bank,
                                                          (unsigned int) program);
    if (status != FLUID_OK) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Could not set soundfont %s", soundFont);
    } else {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Set soundfont %s", soundFont);
    }

    env->ReleaseStringUTFChars(soundFont_, soundFont);
}


extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_setVelocityNative(JNIEnv *env, jobject instance,
                                       jint velocity, jint channel) {
    ENTRY
    fluid_synth_cc(fluid_synth, channel, 7, velocity);
}


extern "C"


JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_stopNoteNative(JNIEnv *env, jobject instance,
                                       jint midiVal, jint channel) {
    ENTRY
    fluid_synth_noteoff(fluid_synth, channel, midiVal);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_playFileNative(JNIEnv *env, jobject instance) {
    ENTRY
    if (fluid_player != NULL) {
        fluid_player_play(fluid_player);
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_stopFileNative(JNIEnv *env, jobject instance) {
    ENTRY
    if (fluid_player != NULL) {
        fluid_player_stop(fluid_player);
        delete_fluid_player(fluid_player);
        fluid_player = NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_stopAllNotesNative(JNIEnv *env, jobject instance, jint channel) {
    ENTRY
    if (fluid_synth != NULL) {
        fluid_synth_all_notes_off(fluid_synth, channel);
    }
}


std::vector<fluid_preset_t> getPresets(fluid_sfont_t *font) {
    font->iteration_start(font);
    std::vector<fluid_preset_t> array(0);
    int idx = 0;
    int available = 0;
    do {
        fluid_preset_t next;
        available = font->iteration_next(font, &next);
        if (available) {
            if (idx >= array.size()) {
                array.resize(array.size() + 1);
            }
            array[idx++] = next;
        }
    } while (available != 0);
    return array;
}


extern "C"
JNIEXPORT jobjectArray JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_getPresetsNative(JNIEnv *env, jobject instance,
                                         jstring soundFont_) {
    const char *soundFont = env->GetStringUTFChars(soundFont_, 0);

    ENTRY
    fluid_sfont_t *sfont = fluid_synth_get_sfont_by_name(fluid_synth, soundFont);

    ::std::vector<fluid_preset_t> presets = getPresets(sfont);

    const char * classname = getClass("Preset");
    __android_log_print(ANDROID_LOG_ERROR, TAG, "classname %s", classname);

    jclass presetClass = env->FindClass(classname);
    if (env->ExceptionCheck()) {
        return NULL;
    }
    jmethodID constructor = env->GetMethodID(presetClass, "<init>", "(Ljava/lang/String;II)V");

    jobjectArray resLines = (jobjectArray) env->NewObjectArray(presets.size(), presetClass, 0);

    for (int i = 0; i < presets.size(); i++) {
        fluid_preset_t preset = presets[i];
        jstring name = env->NewStringUTF(preset.get_name(&preset));

        // preset numbers start at 0, whereas ours start at 1
        jobject newPreset = env->NewObject(presetClass, constructor, name,
                                           preset.get_banknum(&preset),
                                           preset.get_num(&preset) + 1);
        __android_log_print(ANDROID_LOG_DEBUG, TAG, "Loaded preset %s bank %d num %d",
                            preset.get_name(&preset), preset.get_banknum(&preset),
                            preset.get_num(&preset));
        env->SetObjectArrayElement(resLines, i, newPreset);

    }
    env->ReleaseStringUTFChars(soundFont_, soundFont);

    return resLines;
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_initSoundFonts(JNIEnv *env, jobject instance, jobjectArray soundFonts) {

    initFluidState(env, soundFonts);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_loadFileNative(JNIEnv *env, jobject instance,
                                       jstring fullPath_) {
    const char *fullPath = env->GetStringUTFChars(fullPath_, 0);

    ENTRY
    if (fluid_player != NULL) {
        delete_fluid_player(fluid_player);
    }
    fluid_player = new_fluid_player(fluid_synth);
    fluid_player_add(fluid_player, fullPath);
    env->ReleaseStringUTFChars(fullPath_, fullPath);
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_isPlayingNative(JNIEnv *env, jobject instance) {
    ENTRY
    return (jboolean) (fluid_player != NULL &&
                       fluid_player_get_status(fluid_player) == FLUID_PLAYER_PLAYING);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_assignSoundFontNative(JNIEnv *env, jobject instance, jint channel,
                                              jstring soundFontPath_) {
    const char *soundFontPath = env->GetStringUTFChars(soundFontPath_, 0);

    fluid_sfont_t *sFont = fluid_synth_get_sfont_by_name(fluid_synth, soundFontPath);
    if (sFont != NULL) {
        int status = fluid_synth_sfont_select(fluid_synth, channel, sFont->id);
        if (status != FLUID_OK) {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "Could not assign soundfont %s id %d",
                                sFont->get_name(sFont), sFont->id);
        } else {
            __android_log_print(ANDROID_LOG_DEBUG, TAG, "Assigned soundfont %s (id %d) to %d",
                                sFont->get_name(sFont), sFont->id, channel);
        }
    } else {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Could not find soundfont %s", soundFontPath);
    }

    env->ReleaseStringUTFChars(soundFontPath_, soundFontPath);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_unloadDriver(JNIEnv *env, jobject instance) {
    delete_fluid_audio_driver(fluid_audio_driver);
}

extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_loadDriver(JNIEnv *env, jobject instance) {
    fluid_audio_driver = new_fluid_audio_driver(fluid_settings, fluid_synth);
}extern "C"
JNIEXPORT void JNICALL
Java_org_philblandford_fluidkeyboard_fluid_FluidWrapper_releaseAssets(JNIEnv *env, jobject instance) {

    if (fluid_audio_driver != NULL) {
        delete_fluid_audio_driver(fluid_audio_driver);
        fluid_audio_driver = NULL;
    }
    if (fluid_player != NULL) {
        delete_fluid_player(fluid_player);
        fluid_player = NULL;
    }
    if (fluid_synth != NULL) {
        delete_fluid_synth(fluid_synth);
        fluid_synth = NULL;
    }
    if (fluid_settings != NULL) {
        delete_fluid_settings(fluid_settings);
        fluid_settings = NULL;
    }
}