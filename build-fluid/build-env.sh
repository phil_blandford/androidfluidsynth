#!/bin/bash

export DEV=`pwd`
export PREFIX="$DEV/outputs"
export TOOLCHAIN_REPO=https://dl.google.com/android/repository
export TOOLCHAIN_VERSION=r16b
export TOOLCHAIN_SYS=linux-x86_64
export TOOLCHAIN_SUFFIX="${TOOLCHAIN_VERSION}-${TOOLCHAIN_SYS}"
export TOOLCHAIN_DIR="android-ndk-$TOOLCHAIN_VERSION"
export TOOLCHAIN_ZIP="android-ndk-$TOOLCHAIN_SUFFIX.zip"
export TOOLCHAIN_URL="$TOOLCHAIN_REPO/$TOOLCHAIN_ZIP"
export PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
export GCC_VER=4.9
export BUILD_SYS=x86_64-linux-gnu
export ANDROID_API=27
export ANDROID_ARCH=arm
export ANDROID_TARGET=armv5te-none-linux-androideabi
export TOOLCHAIN=arm-linux-androideabi
export NDK_STANDALONE=${DEV}/android-ndk
export NDK_TOOLCHAIN=${DEV}/android-ndk-toolchain
export SYSROOT=${NDK_STANDALONE}/sysroot
export CROSS_PREFIX=${NDK_STANDALONE}/bin/${TOOLCHAIN}
export AR=${CROSS_PREFIX}-ar
export AS=${CROSS_PREFIX}-as
export LD=${CROSS_PREFIX}-ld
export NM=${CROSS_PREFIX}-nm
export CC=${CROSS_PREFIX}-gcc
export CXX=${CROSS_PREFIX}-g++
export CPP=${CROSS_PREFIX}-cpp
export CXXCPP=${CROSS_PREFIX}-cpp
export STRIP=${CROSS_PREFIX}-strip
export RANLIB=${CROSS_PREFIX}-ranlib
export STRINGS=${CROSS_PREFIX}-strings
export CFLAGS="--sysroot=${SYSROOT} -I${SYSROOT}/usr/include -I${PREFIX}/include -fPIE -DANDROID -Wno-multichar"
export CXXFLAGS=${CFLAGS}
export CPPFLAGS="--sysroot=${SYSROOT} -I${SYSROOT}/usr/include -I${NDK_TOOLCHAIN}/include/c++/ -DANDROID -DNO_XMALLOC -mandroid"
export LIBS="-lc"
export LDFLAGS="-Wl,-rpath-link=-I${SYSROOT}/usr/lib -L${SYSROOT}/usr/lib -L${PREFIX}/lib -L${NDK_TOOLCHAIN}/lib"

