#!/bin/bash

. ../build-env.sh

FLUIDSYNTH_DIR=fluidsynth/fluidsynth

build_fluidsynth() {
	git clone https://github.com/atsushieno/fluidsynth.git
	cd $FLUIDSYNTH_DIR
	touch README
	sed -i -e 's/m4_define(\[lt_current\], \[6\])/m4_define([lt_current], [5])/' configure.ac
	sed -i -e 's/m4_define(\[lt_revision\], \[1\])/m4_define([lt_revision], [2])/' configure.ac
	sed -i -e 's/m4_define(\[lt_age\], \[7\])/m4_define([lt_age], [1])/' configure.ac
	./autogen.sh
	./configure --build=${BUILD_SYS} --host=${TOOLCHAIN} --prefix=${PREFIX} --disable-pulse-support --disable-alsa-support --disable-dbus-support
	cp include/fluidsynth.cmake include/fluidsynth.h
	sed -i -e 's/#cmakedefine01 BUILD_SHARED_LIBS/#define BUILD_SHARED_LIBS 0/' include/fluidsynth.h
	echo "#define DEFAULT_SOUNDFONT \"/tmp/default.sf2\"" >> src/synth/fluid_synth.h
	make && make install
	cd --
}

build_fluidsynth || exit

