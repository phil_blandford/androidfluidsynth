#!/bin/bash

. ../build-env.sh

LIBICONV_VER=1.15
LIBICONV_DIR=libiconv-$LIBICONV_VER
LIBICONV_FILE=$LIBICONV_DIR.tar.gz
LIBICONV_URL=https://ftp.gnu.org/pub/gnu/libiconv/$LIBICONV_FILE

LIBFFI_VER=3.2.1
LIBFFI_DIR=libffi-$LIBFFI_VER
LIBFFI_FILE=$LIBFFI_DIR.tar.gz
LIBFFI_URL=ftp://sourceware.org/pub/libffi/$LIBFFI_FILE

GETTEXT_VER=0.19.8.1
GETTEXT_DIR=gettext-$GETTEXT_VER
GETTEXT_FILE=$GETTEXT_DIR.tar.xz
GETTEXT_URL=http://ftp.gnu.org/pub/gnu/gettext/$GETTEXT_FILE

GLIB_VER_MAJ=2.57
GLIB_VER=2.57.1
GLIB_DIR=glib-$GLIB_VER
GLIB_FILE=$GLIB_DIR.tar.xz
GLIB_URL=https://ftp.gnome.org/pub/gnome/sources/glib/$GLIB_VER_MAJ/$GLIB_FILE

get_toolchain() {
	if [ ! -d $NDK_TOOLCHAIN ]
	then
		if [ ! -f $TOOLCHAIN_ZIP ]
		then
			wget $TOOLCHAIN_URL || return $?
		fi
		unzip $TOOLCHAIN_ZIP || return $?
		mv $TOOLCHAIN_DIR $NDK_TOOLCHAIN
	fi
}

prepare_outputs() {
	if [ ! -d $PREFIX ]
	then
		mkdir $PREFIX
	fi
}

make_standalone() {
	if [ ! -d $NDK_STANDALONE ]
	then
	 ${NDK_TOOLCHAIN}/build/tools/make-standalone-toolchain.sh --toolchain=arm-linux-androideabi-4.9 \
	 --stl=gnustl \
	 --arch=arm \
	 --package-dir=$DEV \
	 --install-dir=$NDK_STANDALONE \
	 --force \
	 --platform=android-27
	fi
}

install_packages() {
	sudo apt-get install build-essential
	sudo apt-get install pkg-config automake autoconf libtool
	sudo apt-get install zlib1g-dev libffi-dev libtool-bin
}

build_libiconv() {
	ret=0
	if [ ! -f $PREFIX/lib/libiconv.a ]
	then
		if [ ! -d $LIBICONV_DIR ]
		then
			wget $LIBICONV_URL
			tar -zxvf $LIBICONV_FILE
		fi
		cd $LIBICONV_DIR
		./configure --build=${BUILD_SYS} --host=arm-eabi --prefix=${PREFIX} --disable-rpath --enable-static
		make && make install
		ret=$?
		cd -
	fi
	return $ret
}

build_libffi() {
	ret=0
	if [ ! -f $PREFIX/lib/libffi.a ]
	then
		if [ ! -d $LIBFFI_DIR ]
		then
			wget $LIBFFI_URL
			tar -zxvf $LIBFFI_FILE
		fi
		cd $LIBFFI_DIR
		./configure --build=${BUILD_SYS} --host=arm-eabi --prefix=${PREFIX} --enable-static
		make && make install && mv $PREFIX/lib/$LIBFFI_DIR/include/* $PREFIX/include
		ret=$?
		cd -
	fi
	return $ret
}

#define HAVE_LANGINFO_CODESET 1
#define HAVE_LANGINFO_H 1
#define HAVE_RAW_DECL_NL_LANGINFO 1

patch_gettext() {
	sed -i -e s'/#define HAVE_DECL_FGETS_UNLOCKED 1/#define HAVE_DECL_FGETS_UNLOCKED 0/' gettext-*/config.h
	sed -i -e s'/#define HAVE_DECL_FPUTS_UNLOCKED 1/#define HAVE_DECL_FPUTS_UNLOCKED 0/' gettext-*/config.h
	sed -i -e s'/#define HAVE_LANGINFO_CODESET 1/#define HAVE_LANGINFO_CODESET 0/' gettext-*/config.h
	sed -i -e s'/#define HAVE_LANGINFO_H 1/#define HAVE_LANGINFO_H 0/' gettext-*/config.h
	sed -i -e s'/#define HAVE_RAW_DECL_NL_LANGINFO 1/#define HAVE_RAW_DECL_NL_LANGINFO 0/' gettext-*/config.h
	sed -i -e s'/CFLAGS = /CFLAGS = -D_SPAWN_H_ /' gettext-tools/gnulib-lib/Makefile
	sed -i -e s'/HAVE_POSIX_SPAWNATTR_T = 1/HAVE_POSIX_SPAWNATTR_T = 0/' gettext-tools/gnulib-lib/Makefile
	sed -i -e s'/HAVE_POSIX_SPAWN_FILE_ACTIONS_T = 1/HAVE_POSIX_SPAWN_FILE_ACTIONS_T = 0/' gettext-tools/gnulib-lib/Makefile
	sed -i -e s'/fullname = pwd->pw_gecos;/fullname = "android";/' gettext-tools/src/msginit.c
}

build_gettext() {
	ret=0
	if [ ! -f $PREFIX/lib/libgettextlib.la ]
	then
		if [ ! -d $GETTEXT_DIR ]
		then
			wget $GETTEXT_URL
			tar -xvf $GETTEXT_FILE
		fi
		cd $GETTEXT_DIR
		./configure --build=${BUILD_SYS} --host=arm-eabi --prefix=${PREFIX} --disable-threads --enable-static
		patch_gettext
		make && make install
		ret=$?
		cd -
	fi
	return $ret
}

make_cache_file() {
	cat <<EOF > android.cache
glib_cv_long_long_format=ll
glib_cv_stack_grows=no
glib_cv_sane_realloc=yes
glib_cv_have_strlcpy=no
glib_cv_va_val_copy=yes
glib_cv_rtldglobal_broken=no
glib_cv_uscore=no
glib_cv_monotonic_clock=no
ac_cv_func_nonposix_getpwuid_r=no
ac_cv_func_posix_getpwuid_r=no
ac_cv_func_posix_getgrgid_r=no
glib_cv_use_pid_surrogate=yes
ac_cv_func_printf_unix98=no
ac_cv_func_vsnprintf_c99=yes
ac_cv_func_realloc_0_nonnull=yes
ac_cv_func_realloc_works=yes
EOF
chmod a-x android.cache
}

patch_glib() {
	sed -i -e s'/#define HAVE_LANGINFO_CODESET 1/#undef HAVE_LANGINFO_CODESET/' config.h
	sed -i -e s'/#define HAVE_LANGINFO_TIME 1/#undef HAVE_LANGINFO_TIME/' config.h
	sed -i -e s'/#define HAVE_LANGINFO_OUTDIGIT 1/#undef HAVE_LANGINFO_OUTDIGIT/' config.h
	sed -i -e s'/#define HAVE_LANGINFO_ALTMON 1/#undef HAVE_LANGINFO_ALTMON/' config.h
	sed -i -e s'/#define HAVE_LANGINFO_ABALTMON 1/#undef HAVE_LANGINFO_ABALTMON/' config.h
	sed -i -e s'/#define HAVE_STRTOD_L 1/#undef HAVE_STRTOD_L/' config.h
}

build_glib() {
	ret=0
	if [ ! -f $PREFIX/lib/libglib-2.0.so ]
	then
		if [ ! -d $GLIB_DIR ]
		then
			wget $GLIB_URL
			tar -xvf $GLIB_FILE
		fi
		cd $GLIB_DIR
		make_cache_file
		./autogen.sh
		./configure --build=${BUILD_SYS} --host=${TOOLCHAIN} --prefix=${PREFIX} \
		--disable-dependency-tracking --cache-file=android.cache --enable-included-printf \
		--with-pcre=no --enable-libmount=no --enable-static
		patch_glib
		make && make install
		ret=$?
		cd -
	fi
	return $ret
}

prepare_outputs || exit
get_toolchain || exit
make_standalone || exit
install_packages || exit
build_libiconv || exit
build_libffi || exit
build_gettext || exit
build_glib || exit

