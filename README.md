# AndroidFluidsynth

## Acknowledgements:

The fork of fluidsyth used is one by Atsushi Eno, contained here:

https://github.com/atsushieno/fluidsynth

It contains an OpenSLES driver required to produce audio on Android.


I am indebted to the instructions here:

http://zwyuan.github.io/2016/07/17/cross-compile-glib-for-android/

for building glib for Android, though I have modified them to work with the Android NDK r16.

## Contents

This package Contains:

Script to download and build glib, and its dependencies, for Android

Script to download and build fluidsynth for Android

A toy Android app to demonstrate fluidsynth working on Android

The app should 'just work' if you open it in Android studio and build it - it contains the necessary pre-compiled libraries
and a JNI wrapper. It is HIGHLY recommend to use the NDK r16 - r17 seems to have some strange issues, anything earlier 
is not tested and not guaranteed to work.

It's a work in progress, at the moment it just shows you a keyboard to play, I will add MIDI file playback soon.


## Building GLIB and Fluidsynth

Note - these scripts are Debian (Ubuntu, Mint etc) only at the moment, though if you hack the install_packages() function to 
something appropriate to your system, the rest should work

To build glib for yourself, just:

cd build-fluid

mkdir build

cd build

../build-glib


Then to build fluidsynth:


../build-fluid

To modify fluidsynth, you might want the following workflow:

cd build-fluid/build/fluidsynth/fluidsynth

** do stuff **

cd ../..

../build-fluid

cp outputs/lib/libfluidsynth.so ${PATH_TO_ANDROID_APP}/app/src/main/cpp/fluid/libs/armeabi-v7a/lib


